package com.nespresso.exercise.gantt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Task {
    private String name;
    private int startWeek;
    private int lastingTime;
    private List<Task> dependencies = new ArrayList<>();

    public static final Comparator<Task> FINICHING_TIME_COMPARATOR = new Comparator<Task>() {
        @Override
        public int compare(Task task1, Task task2) {
            if (task1.duration() > task2.duration())
                return 1;
            else if (task1.duration() < task2.duration())
                return -1;
            return 0;
        }
    };

    public Task(String name, int startWeek, int lastingTime) {
        this.name = name;
        this.startWeek = startWeek;
        this.lastingTime = lastingTime;
    }

    public static Task taskWithDependencies(String name, int startWeek, int lastingTime, List<Task> dependencies) {
        Collections.sort(dependencies, FINICHING_TIME_COMPARATOR.reversed());
        Task lastTask = dependencies.get(0);
        Task task = new Task(name, startWeek + lastTask.duration(), lastingTime);
        task.dependencies = dependencies;
        return task;
    }

    public int duration() {
        return startWeek + lastingTime;
    }

    public String plan(int duration) {
        final String firstLetterToCapital = name.substring(0, 1).toUpperCase();
        StringBuilder planBuilder = new StringBuilder();
        for (int index = 0; index < duration; index++) {
            final String representation = (index >= startWeek && index < duration()) ? firstLetterToCapital : ".";
            planBuilder.append(representation);
        }
        return planBuilder.toString();
    }

    @Override
    public String toString() {
        return name;
    }

}
