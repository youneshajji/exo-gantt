package com.nespresso.exercise.gantt;

public class CyclicDependencyException extends RuntimeException {

    public CyclicDependencyException() {
        super();
    }

    public CyclicDependencyException(String message) {
        super(message);
    }


}
