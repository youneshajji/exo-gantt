package com.nespresso.exercise.gantt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Gantt {
    private Map<String, Task> tasks = new LinkedHashMap<>();
    private int duration = 0;

    public void addTask(String taskName, int startWeek, int endWeek) {
        final Task task = new Task(taskName, startWeek, endWeek);
        putTask(taskName, task);
    }

    public void addTask(String taskName, int startWeek, int lastingTime, String... dependenciesTask) {
        List<Task> dependencies = tasksList(dependenciesTask);
        Task taskWithDependencies = Task.taskWithDependencies(taskName, startWeek, lastingTime, dependencies);
        putTask(taskName, taskWithDependencies);
    }

    private void putTask(String taskName, Task taskWithDependencies) {
        checkNotCycled(taskName);
        tasks.put(taskName, taskWithDependencies);
        duration = Math.max(duration, taskWithDependencies.duration());
    }

    private void checkNotCycled(String taskName) {
        if(tasks.containsKey(taskName)) {
            StringBuilder builder = new StringBuilder("Cycle detected : ").append(taskName);
            final List<Task> tasksList = new LinkedList<>(tasks.values());
            Collections.reverse(tasksList);
            for (Task task : tasksList) {
                builder.append(" -> ").append(task);
            }
            throw new CyclicDependencyException(builder.toString());
        }
    }

    private List<Task> tasksList(String... dependenciesTask) {
        List<Task> dependencies = new ArrayList<>();
        for (String taskName : dependenciesTask) {
            dependencies.add(tasks.get(taskName));
        }
        return dependencies;
    }

    public String showPlanFor(String task) {
        return tasks.get(task).plan(duration);
    }

}