package com.nespresso.exercise.gantt;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class GanttTest {

    private Gantt gantt;

    @Before
    public void initGantt() {
        gantt = new Gantt();
    }

    @Test
    public void shouldShowOccupationPerWeekWithFirstLetterUppercase() {
        gantt.addTask("install dev",0,5); //Installation starts now and takes 5 weeks
        assertThat(gantt.showPlanFor("install dev"), is("IIIII"));
        gantt.addTask("governance", 0, 5); //Governance starts now and lasts 5 weeks
        assertThat(gantt.showPlanFor("governance"), is("GGGGG"));
    }

    @Test
    public void shouldShowWaitingSegmentIfTaskStartsLater() {
        gantt.addTask("analysis",3,4); //Analysis starts from week 3 and lasts 4 weeks
        assertThat(gantt.showPlanFor("analysis"), is("...AAAA"));
    }

    @Test
    public void shouldExtendTimelineIfAnotherTaskLastsLonger() {
        gantt.addTask("analysis",0,4);
        assertThat(gantt.showPlanFor("analysis"), is("AAAA"));
        gantt.addTask("development", 3, 6);
        assertThat(gantt.showPlanFor("analysis"),       is("AAAA....."));
        assertThat(gantt.showPlanFor("development"),    is("...DDDDDD"));
    }

    @Test
    public void shouldDelayDueToDependency() {
        gantt.addTask("analysis", 0, 3);
        gantt.addTask("development",0,4,"analysis");
        assertThat(gantt.showPlanFor("analysis"),       is("AAA...."));
        assertThat(gantt.showPlanFor("development"),    is("...DDDD"));
    }

    @Test
    public void shouldDelayDueToWorstDependency() {
        gantt.addTask("analysis",0,3);
        gantt.addTask("development",2,3);
        gantt.addTask("test", 0, 2, "analysis", "development");
        assertThat(gantt.showPlanFor("analysis"),       is("AAA...."));
        assertThat(gantt.showPlanFor("development"),    is("..DDD.."));
        assertThat(gantt.showPlanFor("test"),           is(".....TT"));
    }

    @Test
    public void shouldApplyDelayRelativeToDependency() {
        gantt.addTask("test",0,4);
        gantt.addTask("rollout",2,2,"test");
        assertThat(gantt.showPlanFor("test"),       is("TTTT...."));
        assertThat(gantt.showPlanFor("rollout"),    is("......RR"));
    }

    @Test
    public void shouldWorkOverall() {
        gantt.addTask("analysis", 0, 2);
        gantt.addTask("budgeting", 0, 2, "analysis");
        gantt.addTask("design", 1, 4);
        gantt.addTask("development", 1, 4, "design", "budgeting");
        gantt.addTask("test", 0, 3, "development");
        gantt.addTask("rollout", 1, 2, "test");

        assertThat(gantt.showPlanFor("analysis"),       is("AA.............."));
        assertThat(gantt.showPlanFor("budgeting"),      is("..BB............"));
        assertThat(gantt.showPlanFor("design"),         is(".DDDD..........."));
        assertThat(gantt.showPlanFor("development"),    is("......DDDD......"));
        assertThat(gantt.showPlanFor("test"),           is("..........TTT..."));
        assertThat(gantt.showPlanFor("rollout"),        is("..............RR"));
    }

    @Test
    public void shouldDetectDependencyCycles() {
        gantt.addTask("analysis",0,2);
        gantt.addTask("design",0,2,"analysis");
        gantt.addTask("development",0,2,"design");
        try {
            gantt.addTask("analysis",0,2,"development");
            fail("should not pass");
        } catch (CyclicDependencyException e) {
            assertThat(e.getMessage(), is("Cycle detected : analysis -> development -> design -> analysis"));
        }

    }

}
